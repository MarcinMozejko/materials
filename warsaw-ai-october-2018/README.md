# [Warsaw AI 2018](https://warsaw.ai/speaker/marcin-mozejko/?fbclid=IwAR10YEXwHkhF_V998hyVpfkwRHV8Krx3ljWBwWGL3H4vErfENw211LX5Yn0) 

Conference materials including:
- presentation,
- code for experiments on uncertainties,
- code for experiments on ensembles.
